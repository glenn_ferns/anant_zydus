# API SPECIFICATIONS

## BO APIs

### Sales Summary

This endpoint will return all data related to the 4 tiles available to the BO
- Primary Sales
- Secondary Sales
- Sales Planned
- RCPA

#### URL

```
/BO/Dashboard/SalesSummary
```

#### Method

POST

#### Data Params

##### Required:

- user_id         = current logged in user
- display_user    = user whose data you want to see
- period          = mtd,qtd,ytd
- month           = month selected
- year           = year selected

###### Optional

N.A

#### Success Response

```
{
   "primary_sales":{
      "total_sales":441739,
      "date_range":"Apr-19",
      "sales_target":728976,
      "sales_growth":-15,
      "target_achieved":26
   },
   "secondary_sales":{
      "total_sales":394250,
      "date_range":"Mar-19",
      "target_achieved":74
   },
   "sales_planned":{
      "total_sales":107770,
      "date_range":"Apr-19",
      "sales_target":64832,
      "target_achieved":52
   },
   "rcpa_sales":{
      "total_sales":390380,
      "target_achieved":70,
      "date_range":"Mar-19"
   }
}
```

Code: 200

#### Error Response

```
{}
```

Notes:

________
________

### Efforts

This endpoint will return all data related to the 3 graphs below the tiles

- Total Coverage
- Multivisit Compliance
- Docs RCPAed
  
It will also provide call average figures

#### URL

```
/BO/Dashboard/Efforts
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see
- period          = mtd,qtd,ytd
- month           = month selected
- year            = year selected

###### Optional

N.A

Success Response:

```
{  
   "last_reporting":"25-May-75",
   "total_coverage":5,
   "multivisit_compliance":38,
   "docs_rcpa":63,
   "call_average":32.7
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### Trends

This endpoint will return all trend related data. Trends by default shows 3 month of data. Can be configured

- Sales Summary
- RCPA
- Sales Plan
  

#### URL

```
/BO/Dashboard/Trends
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see

###### Optional

N.A

Success Response:

```
{
   "sales_summary":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":6506649
               },
               {
                  "month":2,
                  "value":2306875
               },
               {
                  "month":3,
                  "value":6085685
               }
            ]
         },
         {
            "label":"Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":24685
               },
               {
                  "month":2,
                  "value":7883248
               },
               {
                  "month":3,
                  "value":3097471
               }
            ]
         },
         {
            "label":"Primary Target",
            "data":[
               {
                  "month":1,
                  "value":5241743
               },
               {
                  "month":2,
                  "value":4115754
               },
               {
                  "month":3,
                  "value":6039104
               }
            ]
         },
         {
            "label":"Achievement",
            "data":[
               {
                  "month":1,
                  "value":43
               },
               {
                  "month":2,
                  "value":19
               },
               {
                  "month":3,
                  "value":98
               }
            ]
         },
         {
            "label":"DOH",
            "data":[
               {
                  "month":1,
                  "value":4
               },
               {
                  "month":2,
                  "value":21
               },
               {
                  "month":3,
                  "value":17
               }
            ]
         },
         {
            "label":"YoY gr",
            "data":[
               {
                  "month":1,
                  "value":22
               },
               {
                  "month":2,
                  "value":6
               },
               {
                  "month":3,
                  "value":93
               }
            ]
         }
      ]
   },
   "rcpa":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"RCPA Sales",
            "data":[
               {
                  "month":1,
                  "value":1381857
               },
               {
                  "month":2,
                  "value":9550141
               },
               {
                  "month":3,
                  "value":2214291
               }
            ]
         },
         {
            "label":"% of Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":2
               },
               {
                  "month":2,
                  "value":70
               },
               {
                  "month":3,
                  "value":59
               }
            ]
         },
         {
            "label":"% of Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":27
               },
               {
                  "month":2,
                  "value":66
               },
               {
                  "month":3,
                  "value":2
               }
            ]
         }
      ]
   },
   "sales_plan":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"Sales Planned",
            "data":[
               {
                  "month":1,
                  "value":9456788
               },
               {
                  "month":2,
                  "value":8278045
               },
               {
                  "month":3,
                  "value":3672516
               }
            ]
         },
         {
            "label":"Sales target",
            "data":[
               {
                  "month":1,
                  "value":8200028
               },
               {
                  "month":2,
                  "value":1835577
               },
               {
                  "month":3,
                  "value":2261354
               }
            ]
         },
         {
            "label":"% Planned",
            "data":[
               {
                  "month":1,
                  "value":8
               },
               {
                  "month":2,
                  "value":58
               },
               {
                  "month":3,
                  "value":20
               }
            ]
         }
      ]
   }
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### Brand Wise Primary Sales

This endpoint gives primary sales figures according to brands

#### URL

```
/BO/PrimarySales/BrandWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"B2373 - Waelchi and Sons",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":27825,
            "value_2":3692,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":66,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":51,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":75,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B3774 - Funk, Fahey and Monahan",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":28033,
            "value_2":36139,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":89,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":90,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":96,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B6728 - Hyatt-Stamm",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":70827,
            "value_2":19087,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":83,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":17,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":14,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B5156 - Wunsch, Wolf and Kiehn",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":33492,
            "value_2":64517,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":46,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":21,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":20,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### SKU Wise Primary Sales

This endpoint gives primary sales figures according to SKUs

#### URL

```
/BO/PrimarySales/SKUWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- brand_id        = brand selected

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"7065482 - Stokes-Leffler - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":40327,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":6,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":19,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":67,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"7046606 - Stokes-Leffler - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":97437,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":50,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":85,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":25,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"4001766 - Stokes-Leffler - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":10334,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":65,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":92,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":60,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"7355387 - Stokes-Leffler - SKU 4",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":72705,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":84,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":84,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### Brand Wise Secondary Sales

This endpoint gives secondary sales figures according to brands

#### URL

```
/BO/SecondarySales/BrandWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"B4894 - Bradtke-Mosciski",
         "secondary_title":"",
         "label_1":"Sec Sales ( Oct-74 )",
         "value_1":67408,
         "type_1":"curr",
         "section_1":{
            "label":"Sec Sales ( Oct-74 )",
            "value":63162,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":52,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Oct-74 )",
            "value":28900,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":23,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B5178 - Powlowski, Graham and Predovic",
         "secondary_title":"",
         "label_1":"Sec Sales ( Oct-74 )",
         "value_1":13173,
         "type_1":"curr",
         "section_1":{
            "label":"Sec Sales ( Oct-74 )",
            "value":57321,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":87,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Oct-74 )",
            "value":47515,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":34,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### SKU Wise Secondary Sales

This endpoint gives secondary sales figures according to SKUs

#### URL

```
/BO/SecondarySales/SKUWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- brand_id        = brand selected

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"6984263 - Legros-Lesch - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Apr-73 ) ",
            "value":11800,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Apr-73 ) ",
            "value":8763,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":26,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"5685116 - Legros-Lesch - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Apr-73 ) ",
            "value":82558,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":18,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Apr-73 ) ",
            "value":2505,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":36,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"1363539 - Legros-Lesch - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Apr-73 ) ",
            "value":21899,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":5,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Apr-73 ) ",
            "value":5438,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":56,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### Sales Plan

This endpoint gives sales planned for last month and 2 perevious months 

#### URL

```
/BO/SalesPlan/Summary
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- month_id        = month of which you want data returned

###### Optional

N.A

Success Response:

```
{
   "hq_target":897729,
   "sales_target":884375,
   "sales_planned":359034,
   "sales_target_planned":69,
   "sc_doc_planned":88,
   "brands_planned":3,
   "sku_planned":44,
   "promoted_sku_planned":83,
   "new_prescribers_planned":5
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### Sales Plan Doctor List

This endpoint gives doctors Sales Plan for the month 

#### URL

```
/BO/SalesPlan/DoctorsList
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- month_id        = month of which you want data returned
- query           = query term in the search box

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "number":34,
         "doctor_id":659810,
         "doctor_name":"Deanna Satterfield",
         "doctor_speciality":"Travel Agent",
         "visit_category":"V2",
         "sales_planned":2985
      },
      {
         "number":90,
         "doctor_id":627589,
         "doctor_name":"Dameon Pouros",
         "doctor_speciality":"Data Processing Equipment Repairer",
         "visit_category":"V2",
         "sales_planned":9263
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### Sales Plan SKU List

This endpoint gives doctors Sales Plan for the month 

#### URL

```
/BO/SalesPlan/SKUList
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- month_id        = month of which you want data returned
- query           = query term in the search box

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "number":72,
         "sku_id":2855559,
         "sku_name":"SKU 1",
         "brand_name":"Veum, Heathcote and Kuvalis",
         "sales_planned":5594
      },
      {
         "number":71,
         "sku_id":906798,
         "sku_name":"SKU 2",
         "brand_name":"Champlin-Wisoky",
         "sales_planned":6394
      },
      {
         "number":59,
         "sku_id":8303625,
         "sku_name":"SKU 3",
         "brand_name":"Kerluke, Marks and Wunsch",
         "sales_planned":389
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### RCPA Doctor Status

This endpoint gives the RCPA doctor statuses

#### URL

```
/BO/RCPA/DoctorStatus
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see
- query_term      = search term typed ( Null by default )

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "doctor_id":7190,
         "doctor_name":"Derek Gislason",
         "doctor_speciality":"Tailor",
         "visit_category":"V3",
         "sales_planned":6366,
         "status":2
      },
      {
         "id":2,
         "doctor_id":6826,
         "doctor_name":"Ms. Charlotte Smith PhD",
         "doctor_speciality":"Radiation Therapist",
         "visit_category":"V1",
         "sales_planned":1757,
         "status":1
      },
      {
         "id":3,
         "doctor_id":8986,
         "doctor_name":"Freda Nikolaus",
         "doctor_speciality":"Bellhop",
         "visit_category":"V2",
         "sales_planned":2953,
         "status":1
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### RCPA Doctor List

This endpoint gives the list of 

- doctors who are RCPAed
- doctors who are not RCPAed

#### URL

```
/BO/RCPA/DoctorList
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see

###### Optional

N.A

Success Response:

```
{
   "doctors_not_rcpaed":[
      {
         "number":82,
         "doctor_id":352359,
         "doctor_name":"Mr. Tyson Glover V",
         "doctor_speciality":"Corporate Trainer",
         "visit_category":"V1",
         "sales_planned":4960,
         "rcpa":null,
         "last_date_visited":2039,
         "no_of_visits":3
      },
      {
         "number":71,
         "doctor_id":787958,
         "doctor_name":"Prof. Dominic Ferry",
         "doctor_speciality":"Software Engineer",
         "visit_category":"V2",
         "sales_planned":2371,
         "rcpa":null,
         "last_date_visited":3392,
         "no_of_visits":2
      },
      {
         "number":46,
         "doctor_id":887060,
         "doctor_name":"Prof. Elouise Crooks",
         "doctor_speciality":"Home Appliance Repairer",
         "visit_category":"V1",
         "sales_planned":5563,
         "rcpa":null,
         "last_date_visited":2743,
         "no_of_visits":1
      }
   ],
   "doctors_rcpaed":[
      {
         "number":35,
         "doctor_id":458481,
         "doctor_name":"Laura Conroy",
         "doctor_speciality":"Transportation Equipment Painters",
         "visit_category":"V3",
         "sales_planned":9523,
         "rcpa":498,
         "last_date_visited":2986,
         "no_of_visits":2
      },
      {
         "number":41,
         "doctor_id":717840,
         "doctor_name":"Dr. Josefa Macejkovic I",
         "doctor_speciality":"Safety Engineer",
         "visit_category":"V2",
         "sales_planned":9776,
         "rcpa":6732,
         "last_date_visited":3553,
         "no_of_visits":1
      },
      {
         "number":62,
         "doctor_id":972985,
         "doctor_name":"Leila Kilback",
         "doctor_speciality":"Pharmaceutical Sales Representative",
         "visit_category":"V3",
         "sales_planned":7472,
         "rcpa":1112,
         "last_date_visited":5179,
         "no_of_visits":2
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### BO Total Coverage

This endpoint gives the list of 

- doctors not met
- doctors met

#### URL

```
/BO/Coverage/TotalCoverage
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see

###### Optional

N.A

Success Response:

```
{
   "doctors_not_met":[
      {
         "number":38,
         "doctor_id":3961,
         "doctor_name":"Timmothy Miller",
         "doctor_speciality":"Order Clerk",
         "visit_category":"V1",
         "sales_planned":1113,
         "rcpa":4662
      },
      {
         "number":21,
         "doctor_id":424492,
         "doctor_name":"Mustafa McGlynn",
         "doctor_speciality":"Securities Sales Agent",
         "visit_category":"V3",
         "sales_planned":2430,
         "rcpa":4123
      }
   ],
   "doctors_met":[
      {
         "number":92,
         "doctor_id":922701,
         "doctor_name":"Verdie Kovacek",
         "doctor_speciality":"Library Assistant",
         "visit_category":"V2",
         "sales_planned":2855,
         "rcpa":3648
      },
      {
         "number":77,
         "doctor_id":307653,
         "doctor_name":"Julianne Kunze",
         "doctor_speciality":"Casting Machine Operator",
         "visit_category":"V3",
         "sales_planned":6559,
         "rcpa":6331
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### BO Multivisit compliance

This endpoint gives the list of 

- doctors with less compliance
- doctors with 100% complaince

#### URL

```
/BO/MultiVisitCompliance/TotalCompliance
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see

###### Optional

N.A

Success Response:

```
{
   "doctors_with_less":[
      {
         "number":52,
         "doctor_id":390680,
         "doctor_name":"Paolo Romaguera",
         "doctor_speciality":"Communication Equipment Repairer",
         "visit_category":"V3",
         "sales_planned":9278,
         "rcpa":4017,
         "last_date_visited":73,
         "no_of_visits":2
      },
      {
         "number":35,
         "doctor_id":360063,
         "doctor_name":"Mrs. Leda Rosenbaum",
         "doctor_speciality":"Life Science Technician",
         "visit_category":"V2",
         "sales_planned":9234,
         "rcpa":7673,
         "last_date_visited":8481,
         "no_of_visits":1
      }
   ],
   "doctors_with_100":[
      {
         "number":61,
         "doctor_id":343112,
         "doctor_name":"Dr. Angie Hoeger",
         "doctor_speciality":"Spotters",
         "visit_category":"V2",
         "sales_planned":4213,
         "rcpa":1158,
         "last_date_visited":9897,
         "no_of_visits":2
      },
      {
         "number":5,
         "doctor_id":359817,
         "doctor_name":"Rahsaan Gleason",
         "doctor_speciality":"Merchandise Displayer OR Window Trimmer",
         "visit_category":"V2",
         "sales_planned":1664,
         "rcpa":2390,
         "last_date_visited":3571,
         "no_of_visits":2
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:



________
________
________

## ABM APIs

### Sales Summary

This endpoint will return all data related to the 4 tiles available to the ABM
- Primary Sales
- Secondary Sales
- RCPA sales
- PCPM

#### URL

```
/ABM/Dashboard/SalesSummary
```

#### Method

POST

#### Data Params

##### Required:

- user_id         = current logged in user
- display_user    = user whose data you want to see
- period          = mtd,qtd,ytd
- month           = month selected
- year           = year selected

###### Optional

N.A

#### Success Response

```
{
   "primary_sales":{
      "total_sales":148472,
      "date_range":"Apr-19",
      "sales_target":342035,
      "sales_growth":6,
      "target_achieved":74
   },
   "secondary_sales":{
      "total_sales":579409,
      "date_range":"Mar-19",
      "target_achieved":32
   },
   "pcpm":{
      "total_sales":480740,
      "date_range":"Apr-19",
      "target_achieved":59
   },
   "rcpa_sales":{
      "total_sales":381244,
      "target_achieved":11,
      "date_range":"Mar-19"
   }
}
```

Code: 200

#### Error Response

```
{}
```

Notes:

________
________

### My Efforts

This endpoint will return all data related to the 3 graphs below the tiles

- My Doc Coverage
- Joint Call Compliance
- No of JFW Days
  
It will also provide call average figures

#### URL

```
/ABM/Dashboard/Efforts
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see
- period          = mtd,qtd,ytd
- month           = month selected
- year            = year selected

###### Optional

N.A

Success Response:

```
{
   "last_reporting":"05-Apr-08",
   "my_doc_coverage":26,
   "joint_call_compliance":90,
   "jfw_days":17,
   "call_average":36.1
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### Trends

This endpoint will return all trend related data. Trends by default shows 3 month of data. Can be configured

- Sales Summary
- RCPA
- Sales Plan
  

#### URL

```
/ABM/Dashboard/Trends
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see

###### Optional

N.A

Success Response:

```
{
   "sales_summary":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":7976616
               },
               {
                  "month":2,
                  "value":6385182
               },
               {
                  "month":3,
                  "value":8411823
               }
            ]
         },
         {
            "label":"Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":8038960
               },
               {
                  "month":2,
                  "value":5569424
               },
               {
                  "month":3,
                  "value":4647792
               }
            ]
         },
         {
            "label":"Primary Target",
            "data":[
               {
                  "month":1,
                  "value":3811141
               },
               {
                  "month":2,
                  "value":7276831
               },
               {
                  "month":3,
                  "value":9078237
               }
            ]
         },
         {
            "label":"Achievement",
            "data":[
               {
                  "month":1,
                  "value":81
               },
               {
                  "month":2,
                  "value":56
               },
               {
                  "month":3,
                  "value":88
               }
            ]
         },
         {
            "label":"DOH",
            "data":[
               {
                  "month":1,
                  "value":57
               },
               {
                  "month":2,
                  "value":11
               },
               {
                  "month":3,
                  "value":33
               }
            ]
         },
         {
            "label":"YoY gr",
            "data":[
               {
                  "month":1,
                  "value":76
               },
               {
                  "month":2,
                  "value":55
               },
               {
                  "month":3,
                  "value":58
               }
            ]
         }
      ]
   },
   "rcpa":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"RCPA Sales",
            "data":[
               {
                  "month":1,
                  "value":9463949
               },
               {
                  "month":2,
                  "value":5681284
               },
               {
                  "month":3,
                  "value":3387124
               }
            ]
         },
         {
            "label":"% of Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":42
               },
               {
                  "month":2,
                  "value":31
               },
               {
                  "month":3,
                  "value":31
               }
            ]
         },
         {
            "label":"% of Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":55
               },
               {
                  "month":2,
                  "value":19
               },
               {
                  "month":3,
                  "value":14
               }
            ]
         }
      ]
   },
   "sales_plan":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"GSP Planned",
            "data":[
               {
                  "month":1,
                  "value":6413875
               },
               {
                  "month":2,
                  "value":2820067
               },
               {
                  "month":3,
                  "value":8786613
               }
            ]
         },
         {
            "label":"GSP Target",
            "data":[
               {
                  "month":1,
                  "value":5895886
               },
               {
                  "month":2,
                  "value":7099418
               },
               {
                  "month":3,
                  "value":9911141
               }
            ]
         },
         {
            "label":"% Planned",
            "data":[
               {
                  "month":1,
                  "value":20
               },
               {
                  "month":2,
                  "value":88
               },
               {
                  "month":3,
                  "value":84
               }
            ]
         }
      ]
   }
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### HQ Wise Primary Sales

This endpoint gives primary sales figures according to HQs

#### URL

```
/ABM/PrimarySales/HQWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"Kozeyhaven",
         "secondary_title":"( # of BOs 7 )",
         "section_1":{
            "label":"Primary Sales",
            "value":6162,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":63,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":69,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":20,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"Lake Destanyport",
         "secondary_title":"( # of BOs 9 )",
         "section_1":{
            "label":"Primary Sales",
            "value":33084,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":61,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":37,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":57,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"South Trey",
         "secondary_title":"( # of BOs 5 )",
         "section_1":{
            "label":"Primary Sales",
            "value":13575,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":81,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":19,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":30,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"Ellisbury",
         "secondary_title":"( # of BOs 9 )",
         "section_1":{
            "label":"Primary Sales",
            "value":64396,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":10,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":86,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":17,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### Area Brand Wise Primary Sales

This endpoint gives primary sales figures according to Brand Wise Area

#### URL

```
/ABM/PrimarySales/BrandWiseArea
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"B1899 - Okuneva-Powlowski",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":94108,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":69,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":11,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":86,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B8261 - Kovacek, Rice and Stark",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":47948,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":93,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":0,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":16,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B5666 - Jacobs PLC",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":23436,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":20,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":94,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":25,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B7790 - Klocko PLC",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":87687,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":25,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":79,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":53,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### Brand Wise Primary Sales

This endpoint gives primary sales figures according to brands

#### URL

```
/ABM/PrimarySales/BrandWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- hq_id           = selected hq

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"B8061 - Watsica and Sons",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":59994,
            "value_2":27752,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":84,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":12,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":90,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B2264 - Ankunding, Gleason and Purdy",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":49391,
            "value_2":65477,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":79,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":34,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":92,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B3547 - Reilly, Streich and Hessel",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":93637,
            "value_2":61143,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":96,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":26,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":26,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B1051 - Hermann, Stehr and Witting",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":49156,
            "value_2":5751,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":99,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":81,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":99,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### SKU Wise Primary Sales

This endpoint gives primary sales figures according to SKUs

#### URL

```
/ABM/PrimarySales/SKUWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- brand_id        = brand selected

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"5722051 - Rempel-Mann - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":50825,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":24,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":83,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":62,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"6169242 - Rempel-Mann - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":4057,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":59,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":48,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":32,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"5634614 - Rempel-Mann - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":59024,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":33,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":16,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":97,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"7524938 - Rempel-Mann - SKU 4",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":9147,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":90,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":48,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":98,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### HQ Wise Secondary Sales

This endpoint gives secondary sales figures according to HQs

#### URL

```
/ABM/SecondarySales/HQWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"Brianaside",
         "secondary_title":"( # of BOs 5 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":29524,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":48,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":33036,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":34,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"Destinville",
         "secondary_title":"( # of BOs 4 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":63650,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":54,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":96851,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":74,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"Hestertown",
         "secondary_title":"( # of BOs 4 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":91487,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":14,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":20951,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":21,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"North Casperchester",
         "secondary_title":"( # of BOs 0 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":30615,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":29126,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":42,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### Area Brand Wise Secondary Sales

This endpoint gives secondary sales figures according to Brand Wise Area

#### URL

```
/ABM/SecondarySales/BrandWiseArea
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"B9076 - Bahringer Ltd",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":63446,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":14,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":895,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":77,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B9644 - Dibbert-Rolfson",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":61091,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":87,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":50000,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":75,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B4385 - Yundt Inc",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":9562,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":34,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":80939,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":96,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B1511 - Kunde, Kohler and Marks",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":93729,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":70,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":21933,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":52,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### Brand Wise Secondary Sales

This endpoint gives secondary sales figures according to brands

#### URL

```
/ABM/SecondarySales/BrandWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- hq_id           = selected hq

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"B9734 - Mueller LLC",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":86821,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":61,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":99848,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":31,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B1730 - Wintheiser, Kiehn and Stehr",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":91673,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":11,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":91760,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":46,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B7976 - Yost-Wintheiser",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":47516,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":71,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":22463,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":16,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B1455 - Haley-Beatty",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":71405,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":64,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":69665,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":75,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### SKU Wise Secondary Sales

This endpoint gives secondary sales figures according to SKUs

#### URL

```
/ABM/SecondarySales/SKUWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- brand_id        = brand selected

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"3737799 - Hickle and Sons - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":59156,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":3,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":34290,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":66,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"2011179 - Hickle and Sons - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":81703,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":62,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":62864,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":71,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"9204453 - Hickle and Sons - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":84266,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":76,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":46181,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":84,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"268868 - Hickle and Sons - SKU 4",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":89544,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":32995,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":96,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________


### BO Wise RCPA Sales

This endpoint gives RCPA sales figures according to BOs

#### URL

```
/ABM/RCPA/BOWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "bo_id":2712,
         "bo_name":"Prof. Polly Fadel",
         "rcpa_sales":45666
      },
      {
         "id":2,
         "bo_id":3830,
         "bo_name":"Dr. Cortez Kuhlman MD",
         "rcpa_sales":69772
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:


### HQ Wise PCPM Sales

This endpoint gives PCPM sales figures according to HQs

#### URL

```
/ABM/PCPM/HQWise
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user

###### Optional

N.A

Success Response:

```
{
   "payload":[
      {
         "id":1,
         "title":"Port Terencemouth",
         "secondary_title":"( # of BOs 2 )",
         "section_1":{
            "label":"Primary Sales",
            "value":63689,
            "value_2":40597,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":619585,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":12065,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":81,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"Maychester",
         "secondary_title":"( # of BOs 1 )",
         "section_1":{
            "label":"Primary Sales",
            "value":39276,
            "value_2":21108,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":446298,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":87392,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":97,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"Jerrodmouth",
         "secondary_title":"( # of BOs 8 )",
         "section_1":{
            "label":"Primary Sales",
            "value":91682,
            "value_2":67973,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":327261,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":74034,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":46,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"Jacobsonmouth",
         "secondary_title":"( # of BOs 4 )",
         "section_1":{
            "label":"Primary Sales",
            "value":32971,
            "value_2":40190,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":632739,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":79290,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":1,
            "type":"perc"
         }
      }
   ]
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________

### Aggregate Efforts

This endpoint will return all aggregated data related to the 3 graphs below the tiles

- Total Coverage
- Multivisit Compliance
- Docs RCPAed
  
It will also provide aggregate call average figures

#### URL

```
/ABM/Dashboard/AggregateEfforts
```

#### Method

POST

#### Data Params

##### Required

- user_id         = current logged in user
- display_user    = user whose data you want to see
- period          = mtd,qtd,ytd
- month           = month selected
- year            = year selected

###### Optional

N.A

Success Response:

```
{  
   "last_reporting":"25-May-75",
   "total_coverage":5,
   "multivisit_compliance":38,
   "docs_rcpa":63,
   "call_average":32.7
}
```

Code: 200 
Error Response:
```
{}
```

Notes:

________
________
