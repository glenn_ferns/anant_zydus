<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class Dashboard extends CI_Controller {
    
    public function FetchDivisions(){
        $faker = Faker\Factory::create();

        $data = [];

        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "name" => $faker->word,
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    public function FetchRegions(){
        $faker = Faker\Factory::create();

        $data = [];

        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "name" => $faker->state,
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    public function FetchRBM(){
        $faker = Faker\Factory::create();

        $data = [];

        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "name" => $faker->name,
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }
}
