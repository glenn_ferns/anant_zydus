<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class SalesPlan extends CI_Controller {
    
    public function Summary(){
        $faker = Faker\Factory::create();
        $data = [];
        $data['hq_target'] = $faker->randomNumber(6);
        $data['sales_target'] = $faker->randomNumber(6);
        $data['sales_planned'] = $faker->randomNumber(6);
        $data['sales_target_planned'] = $faker->randomNumber(2);
        $data['sc_doc_planned'] = $faker->randomNumber(2);
        $data['brands_planned'] = $faker->randomNumber(1);
        $data['sku_planned'] = $faker->randomNumber(2);
        $data['promoted_sku_planned'] = $faker->randomNumber(2);
        $data['new_prescribers_planned'] = $faker->randomNumber(1);
        echo json_encode($data);
    }

    public function DoctorsList(){
        $faker = Faker\Factory::create();
        $data = [];

        for($i=1;$i<10;$i++){
            $array = [
                "number" => $faker->randomNumber(2),
                "doctor_id" => $faker->randomNumber(6),
                "doctor_name" => $faker->name,
                "doctor_speciality" => $faker->jobTitle,
                "visit_category" => "V".$faker->numberBetween(1,3),
                "sales_planned" => $faker->randomNumber(4),
            ];
            array_push($data,$array);
        }

        echo json_encode([
            'payload' => $data
        ]);
    }


    public function SKUList(){
        $faker = Faker\Factory::create();
        $data = [];
        for($i=1;$i<10;$i++){
            $array = [
                "number" => $faker->randomNumber(2),
                "sku_id" => $faker->randomNumber(7),
                "sku_name" => "SKU ".$i,
                "brand_name" => $faker->company,
                "sales_planned" => $faker->randomNumber(4),
            ];
            array_push($data,$array);
        }

        echo json_encode([
            'payload' => $data
        ]);
    }

}
