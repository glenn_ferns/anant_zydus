<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class RCPA extends CI_Controller
{

	public function DoctorStatus()
	{
		$faker = Faker\Factory::create();

		$data = [];

		for ($i = 1; $i < 4; $i++) {
			$array = [
				"id" => $i,
				"doctor_id" => $faker->randomNumber(4),
				"doctor_name" => $faker->name,
				"doctor_speciality" => $faker->jobTitle,
				"visit_category" => "V" . $faker->numberBetween(1, 3),
				"sales_planned" => $faker->randomNumber(4),
				"status" => $faker->numberBetween(1, 2)
			];
			array_push($data, $array);
		}
		echo json_encode([
			'payload' => $data
		]);
	}

	public function SalesList()
	{
		$faker = Faker\Factory::create();

		$data = [];

		for ($i = 1; $i < 10; $i++) {
			$array = [
				"id" => $i,
				"doctor_name" => $faker->name,
				"sales" => $faker->randomNumber(4),
			];
			array_push($data, $array);
		}
		echo json_encode([
			'payload' => $data
		]);
	}

	public function DoctorsList()
	{
		$faker = Faker\Factory::create();

		$data = [];
		$doctors_not_rcpa = [];
		$doctors_rcpa = [];

		for ($i = 1; $i < 4; $i++) {
			$visits = $faker->numberBetween(1, 3);
			$array = [
				"number" => $faker->randomNumber(2),
				"doctor_id" => $faker->randomNumber(6),
				"doctor_name" => $faker->name,
				"doctor_speciality" => $faker->jobTitle,
				"visit_category" => "V" . $visits,
				"sales_planned" => $faker->randomNumber(4),
				"rcpa" => $faker->randomNumber(4),
				"last_date_visited" => $faker->randomNumber(4),
				"no_of_visits" => $visits - 1,
			];
			array_push($doctors_rcpa, $array);
		}

		for ($i = 1; $i < 4; $i++) {
			$visits = $faker->numberBetween(1, 3);
			$array = [
				"number" => $faker->randomNumber(2),
				"doctor_id" => $faker->randomNumber(6),
				"doctor_name" => $faker->name,
				"doctor_speciality" => $faker->jobTitle,
				"visit_category" => "V" . $visits,
				"sales_planned" => $faker->randomNumber(4),
				"rcpa" => null,
				"last_date_visited" => $faker->randomNumber(4),
				"no_of_visits" => $faker->numberBetween(1, 3),
			];
			array_push($doctors_not_rcpa, $array);
		}
		$data['doctors_not_rcpaed'] = $doctors_not_rcpa;
		$data['doctors_rcpaed'] = $doctors_rcpa;

		echo json_encode($data);
	}

}
