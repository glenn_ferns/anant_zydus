<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class MultiVisitCompliance extends CI_Controller
{

	public function TotalCompliance()
	{
		$faker = Faker\Factory::create();

		$data = [];
		$doctors_with_less = [];
		$doctors_with_100 = [];

		for ($i = 1; $i < 3; $i++) {
			$visits = $faker->numberBetween(1, 3);
			$array = [
				"number" => $faker->randomNumber(2),
				"doctor_id" => $faker->randomNumber(6),
				"doctor_name" => $faker->name,
				"doctor_speciality" => $faker->jobTitle,
				"visit_category" => "V" . $visits,
				"sales_planned" => $faker->randomNumber(4),
				"rcpa" => $faker->randomNumber(4),
				"last_date_visited" => $faker->date($format = 'd-M-Y', $max = 'now'),
				"no_of_visits" => $visits - 1,
			];
			array_push($doctors_with_less, $array);
		}

		for ($i = 1; $i < 3; $i++) {
			$visits = $faker->numberBetween(1, 3);
			$array = [
				"number" => $faker->randomNumber(2),
				"doctor_id" => $faker->randomNumber(6),
				"doctor_name" => $faker->name,
				"doctor_speciality" => $faker->jobTitle,
				"visit_category" => "V" . $visits,
				"sales_planned" => $faker->randomNumber(4),
				"rcpa" => $faker->randomNumber(4),
				"last_date_visited" => $faker->date($format = 'd-M-Y', $max = 'now'),
				"no_of_visits" => $visits,
			];
			array_push($doctors_with_100, $array);
		}
		$data['doctors_with_less'] = $doctors_with_less;
		$data['doctors_with_100'] = $doctors_with_100;

		echo json_encode($data);
	}

}
