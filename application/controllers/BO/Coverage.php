<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class Coverage extends CI_Controller {
    
    public function TotalCoverage(){
        $faker = Faker\Factory::create();

        $data = [];
        $doctors_met = [];
        $doctors_not_met = [];

        for($i=1;$i<3;$i++){
            $array = [
                "number" => $faker->randomNumber(2),
                "doctor_id" => $faker->randomNumber(6),
                "doctor_name" => $faker->name,
                "doctor_speciality" => $faker->jobTitle,
                "visit_category" => "V".$faker->numberBetween(1,3),
                "sales_planned" => $faker->randomNumber(4),
                "rcpa" => $faker->randomNumber(4)
            ];
            array_push($doctors_met,$array);
        }

        for($i=1;$i<3;$i++){
            $array = [
                "number" => $faker->randomNumber(2),
                "doctor_id" => $faker->randomNumber(6),
                "doctor_name" => $faker->name,
                "doctor_speciality" => $faker->jobTitle,
                "visit_category" => "V".$faker->numberBetween(1,3),
                "sales_planned" => $faker->randomNumber(4),
                "rcpa" => $faker->randomNumber(4)
            ];
            array_push($doctors_not_met,$array);
        }
        $data['doctors_not_met'] = $doctors_not_met;
        $data['doctors_met'] = $doctors_met;
        
        echo json_encode($data);
    }

}
