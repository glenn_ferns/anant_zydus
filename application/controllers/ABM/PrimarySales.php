<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class PrimarySales extends CI_Controller {
    
    public function HQWise(){
        $faker = Faker\Factory::create();

        $data = [];
        $date = $faker->date($format = 'M-y', $max = 'now');
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => $faker->city,
                "secondary_title" => "( # of BOs ".$faker->randomNumber(1)." )",

                "section_1" => [
                    "label" => "Primary Sales",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_2" => [
                    "label" => "Primary Ach",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_3" => [
                    "label" => "Sec Sales ( ".$date." )",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_4" => [
                    "label" => "Primary YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    public function BrandWiseArea(){
        $faker = Faker\Factory::create();

        $data = [];
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => "B".$faker->randomNumber(4)." - ".$faker->company,
                "secondary_title" => "",

                "section_1" => [
                    "label" => "Primary Sales",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_2" => [
                    "label" => "Achievement",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_3" => [
                    "label" => "% of total sales",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_4" => [
                    "label" => "Primary YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }
    
    
    public function BrandWise(){
        $faker = Faker\Factory::create();

        $data = [];

        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => "B".$faker->randomNumber(4)." - ".$faker->company,
                "secondary_title" => "",

                "section_1" => [
                    "label" => "Primary Sales",
                    "value" => $faker->randomNumber(5),
                    "value_2" => $faker->randomNumber(5),
                    "type" => "curr",
                ],
                "section_2" => [
                    "label" => "Achievement",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
                "section_3" => [
                    "label" => "% of total sales",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
                "section_4" => [
                    "label" => "Primary YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    public function SKUWise(){
        $faker = Faker\Factory::create();

        $data = [];
        $brand = $faker->company;
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => $faker->randomNumber(7)." - ".$brand." - "." SKU ".$i,
                "secondary_title" => "",

                "section_1" => [
                    "label" => "Primary Sales (₹)",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_2" => [
                    "label" => "Achievement",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_3" => [
                    "label" => "% of total sales",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_4" => [
                    "label" => "Primary YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    
}
