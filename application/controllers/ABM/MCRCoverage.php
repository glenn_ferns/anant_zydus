<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class MCRCoverage extends CI_Controller
{
	public function index()
	{
		$faker = Faker\Factory::create();

		$data = [];

		for ($i = 1; $i < 10; $i++) {
			$array = [
				"id" => $i,
				"name" => $faker->name(),
				"value" => $faker->numberBetween(0, 100),
				"date" => $faker->date($format = 'd-M-Y', $max = 'now')
			];
			array_push($data, $array);
		}
		echo json_encode([
			'payload' => $data
		]);
	}
}
