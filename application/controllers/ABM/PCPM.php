<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class PCPM extends CI_Controller {
    
    public function HQWise(){
        $faker = Faker\Factory::create();

        $data = [];
        $date = $faker->date($format = 'M-y', $max = 'now');
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => $faker->city,
                "secondary_title" => "( # of BOs ".$faker->randomNumber(1)." )",

                "section_1" => [
                    "label" => "Primary Sales",
                    "value" => $faker->randomNumber(5),
                    // "value_2" => $faker->randomNumber(5),
                    "type" => "curr",
                ],
                "section_2" => [
                    "label" => "PCPM",
                    "value" => $faker->randomNumber(6),
                    "type" => "curr",
                ],
                "section_3" => [
                    "label" => "Sec Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],
                "section_4" => [
                    "label" => "PCPM YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }
}
