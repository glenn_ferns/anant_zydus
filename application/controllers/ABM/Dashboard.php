<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class Dashboard extends CI_Controller
{

	public function SalesSummary()
	{
		$user_id = $this->input->post('user_id');
		$display_user = $this->input->post('display_user');
		$period = $this->input->post('period');
		$month = $this->input->post('month');
		$year = $this->input->post('year');

		$faker = Faker\Factory::create();
		$data = [
			"primary_sales" => [
				"total_sales" => $faker->randomNumber(6),
				"date_range" => "Apr-19",
				"sales_target" => $faker->randomNumber(6),
				"sales_growth" => $faker->numberBetween(-20, 100),
				"target_achieved" => $faker->randomNumber(2),
			],
			"secondary_sales" => [
				"total_sales" => $faker->randomNumber(6),
				"date_range" => "Mar-19",
				"target_achieved" => $faker->randomNumber(2),
			],
			"pcpm" => [
				"total_sales" => $faker->randomNumber(6),
				"date_range" => "Apr-19",
				"target_achieved" => $faker->randomNumber(2),
			],
			"rcpa_sales" => [
				"total_sales" => $faker->randomNumber(6),
				"target_achieved" => $faker->randomNumber(2),
				"date_range" => "Mar-19",
			],
		];

		echo json_encode($data);
	}

	public function Efforts()
	{
		$faker = Faker\Factory::create();
		$data = [
			"last_reporting" => $faker->date($format = 'd-M-y', $max = 'now'),
			"my_doc_coverage" => $faker->randomNumber(2),
			"joint_call_compliance" => $faker->randomNumber(2),
			"jfw_days" => $faker->numberBetween(1, 30),
			"call_average" => $faker->randomFloat(2, 10, 15) . '',
		];

		echo json_encode($data);
	}

	public function Trends()
	{
		$faker = Faker\Factory::create();
		$data = [
			"sales_summary" => [
				"months" => [1, 2, 3],
				"rows" => [
					[
						"label" => "Primary Sales",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(7)],
							["month" => 2, "value" => $faker->randomNumber(7)],
							["month" => 3, "value" => $faker->randomNumber(7)],
						]
					],
					[
						"label" => "Secondary Sales",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(7)],
							["month" => 2, "value" => $faker->randomNumber(7)],
							["month" => 3, "value" => $faker->randomNumber(7)],
						]
					],
					[
						"label" => "Primary Target",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(7)],
							["month" => 2, "value" => $faker->randomNumber(7)],
							["month" => 3, "value" => $faker->randomNumber(7)],
						]
					],
					[
						"label" => "Achievement",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(2)],
							["month" => 2, "value" => $faker->randomNumber(2)],
							["month" => 3, "value" => $faker->randomNumber(2)],
						]
					],
					[
						"label" => "DOH",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(2)],
							["month" => 2, "value" => $faker->randomNumber(2)],
							["month" => 3, "value" => $faker->randomNumber(2)],
						]
					],
					[
						"label" => "YoY gr",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(2)],
							["month" => 2, "value" => $faker->randomNumber(2)],
							["month" => 3, "value" => $faker->randomNumber(2)],
						]
					],

				]
			],
			"rcpa" => [
				"months" => [1, 2, 3],
				"rows" => [
					[
						"label" => "RCPA Sales",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(7)],
							["month" => 2, "value" => $faker->randomNumber(7)],
							["month" => 3, "value" => $faker->randomNumber(7)],
						]
					],
					[
						"label" => "% of Primary Sales",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(2)],
							["month" => 2, "value" => $faker->randomNumber(2)],
							["month" => 3, "value" => $faker->randomNumber(2)],
						]
					],
					[
						"label" => "% of Secondary Sales",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(2)],
							["month" => 2, "value" => $faker->randomNumber(2)],
							["month" => 3, "value" => $faker->randomNumber(2)],
						]
					],
				]
			],
			"sales_plan" => [
				"months" => [1, 2, 3],
				"rows" => [
					[
						"label" => "GSP Planned",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(7)],
							["month" => 2, "value" => $faker->randomNumber(7)],
							["month" => 3, "value" => $faker->randomNumber(7)],
						]
					],
					[
						"label" => "GSP Target",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(7)],
							["month" => 2, "value" => $faker->randomNumber(7)],
							["month" => 3, "value" => $faker->randomNumber(7)],
						]
					],
					[
						"label" => "% Planned",
						"data" => [
							["month" => 1, "value" => $faker->randomNumber(2)],
							["month" => 2, "value" => $faker->randomNumber(2)],
							["month" => 3, "value" => $faker->randomNumber(2)],
						]
					],
				]
			],

		];

		echo json_encode($data);
	}


	public function AggregateEfforts()
	{
		$faker = Faker\Factory::create();
		$data = [
			"last_reporting" => $faker->date($format = 'd-M-y', $max = 'now'),
			"total_coverage" => $faker->randomNumber(2),
			"multivisit_compliance" => $faker->randomNumber(2),
			"docs_rcpa" => $faker->randomNumber(2),
			"call_average" => $faker->randomFloat(2, 10, 15) . "",
		];
		echo json_encode($data);
	}
}
