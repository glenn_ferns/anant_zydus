<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class RCPA extends CI_Controller {
    
    public function BOWise(){
        $faker = Faker\Factory::create();

        $data = [];

        for($i=1;$i<3;$i++){
            $array = [
                "id" => $i,
                "bo_id" => $faker->randomNumber(4),
                "bo_name" => $faker->name,
                "rcpa_sales" => $faker->randomNumber(5),
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    // To fetch doctors list, we can reuse the BO/RCPA/DoctorStatus

}
