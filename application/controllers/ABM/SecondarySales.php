<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require 'vendor/autoload.php';

class SecondarySales extends CI_Controller {
    
    public function HQWise(){
        $faker = Faker\Factory::create();

        $data = [];
        $date = $faker->date($format = 'M-y', $max = 'now');
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => $faker->city,
                "secondary_title" => "( # of BOs ".$faker->randomNumber(1)." )",

                "section_1" => [
                    "label" => "Sec Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_2" => [
                    "label" => "Sec YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_3" => [
                    "label" => "Primary Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_4" => [
                    "label" => "Sec as % of Primary",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    public function BrandWiseArea(){
        $faker = Faker\Factory::create();

        $data = [];
        $date = $faker->date($format = 'M-y', $max = 'now');
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => "B".$faker->randomNumber(4)." - ".$faker->company,
                "secondary_title" => "",
                "section_1" => [
                    "label" => "Sec Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_2" => [
                    "label" => "Sec YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_3" => [
                    "label" => "Primary Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_4" => [
                    "label" => "Sec as % of Primary",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }
    
    
    public function BrandWise(){
        $faker = Faker\Factory::create();

        $data = [];
        $date = $faker->date($format = 'M-y', $max = 'now');
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => "B".$faker->randomNumber(4)." - ".$faker->company,
                "secondary_title" => "",

                "section_1" => [
                    "label" => "Sec Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_2" => [
                    "label" => "Sec YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_3" => [
                    "label" => "Primary Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_4" => [
                    "label" => "Sec as % of Primary",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }

    public function SKUWise(){
        $faker = Faker\Factory::create();

        $data = [];
        $brand = $faker->company;
        $date = $faker->date($format = 'M-y', $max = 'now');
        for($i=1;$i<5;$i++){
            $array = [
                "id" => $i,
                "title" => $faker->randomNumber(7)." - ".$brand." - "." SKU ".$i,
                "secondary_title" => "",

                "section_1" => [
                    "label" => "Sec Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_2" => [
                    "label" => "Sec YoY gr",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],

                "section_3" => [
                    "label" => "Primary Sales ( ".$date." ) ",
                    "value" => $faker->randomNumber(5),
                    "type" => "curr",
                ],

                "section_4" => [
                    "label" => "Sec as % of Primary",
                    "value" => $faker->randomNumber(2),
                    "type" => "perc",
                ],
            ];
            array_push($data,$array);
        }
        echo json_encode([
            'payload' => $data
        ]);
    }
}
