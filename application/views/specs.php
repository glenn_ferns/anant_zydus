<h1 id="apispecifications">API SPECIFICATIONS</h1>

<h2 id="boapis">BO APIs</h2>

<h3 id="salessummary">Sales Summary</h3>

<p>This endpoint will return all data related to the 4 tiles available to the BO</p>

<ul>
<li>Primary Sales</li>

<li>Secondary Sales</li>

<li>Sales Planned</li>

<li>RCPA</li>
</ul>

<h4 id="url">URL</h4>

<pre><code>/BO/Dashboard/SalesSummary
</code></pre>

<h4 id="method">Method</h4>

<p>POST</p>

<h4 id="dataparams">Data Params</h4>

<h5 id="required">Required:</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>

<li>period          = mtd,qtd,ytd</li>

<li>month           = month selected</li>

<li>year           = year selected</li>
</ul>

<h6 id="optional">Optional</h6>

<p>N.A</p>

<h4 id="successresponse">Success Response</h4>

<pre><code>{
   "primary_sales":{
      "total_sales":441739,
      "date_range":"Apr-19",
      "sales_target":728976,
      "sales_growth":-15,
      "target_achieved":26
   },
   "secondary_sales":{
      "total_sales":394250,
      "date_range":"Mar-19",
      "target_achieved":74
   },
   "sales_planned":{
      "total_sales":107770,
      "date_range":"Apr-19",
      "sales_target":64832,
      "target_achieved":52
   },
   "rcpa_sales":{
      "total_sales":390380,
      "target_achieved":70,
      "date_range":"Mar-19"
   }
}
</code></pre>

<p>Code: 200</p>

<h4 id="errorresponse">Error Response</h4>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="efforts">Efforts</h3>

<p>This endpoint will return all data related to the 3 graphs below the tiles</p>

<ul>
<li>Total Coverage</li>

<li>Multivisit Compliance</li>

<li>Docs RCPAed</li>
</ul>

<p>It will also provide call average figures</p>

<h4 id="url-1">URL</h4>

<pre><code>/BO/Dashboard/Efforts
</code></pre>

<h4 id="method-1">Method</h4>

<p>POST</p>

<h4 id="dataparams-1">Data Params</h4>

<h5 id="required-1">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>

<li>period          = mtd,qtd,ytd</li>

<li>month           = month selected</li>

<li>year            = year selected</li>
</ul>

<h6 id="optional-1">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{  
   "last_reporting":"25-May-75",
   "total_coverage":5,
   "multivisit_compliance":38,
   "docs_rcpa":63,
   "call_average":32.7
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="trends">Trends</h3>

<p>This endpoint will return all trend related data. Trends by default shows 3 month of data. Can be configured</p>

<ul>
<li>Sales Summary</li>

<li>RCPA</li>

<li>Sales Plan</li>
</ul>

<h4 id="url-2">URL</h4>

<pre><code>/BO/Dashboard/Trends
</code></pre>

<h4 id="method-2">Method</h4>

<p>POST</p>

<h4 id="dataparams-2">Data Params</h4>

<h5 id="required-2">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>
</ul>

<h6 id="optional-2">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "sales_summary":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":6506649
               },
               {
                  "month":2,
                  "value":2306875
               },
               {
                  "month":3,
                  "value":6085685
               }
            ]
         },
         {
            "label":"Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":24685
               },
               {
                  "month":2,
                  "value":7883248
               },
               {
                  "month":3,
                  "value":3097471
               }
            ]
         },
         {
            "label":"Primary Target",
            "data":[
               {
                  "month":1,
                  "value":5241743
               },
               {
                  "month":2,
                  "value":4115754
               },
               {
                  "month":3,
                  "value":6039104
               }
            ]
         },
         {
            "label":"Achievement",
            "data":[
               {
                  "month":1,
                  "value":43
               },
               {
                  "month":2,
                  "value":19
               },
               {
                  "month":3,
                  "value":98
               }
            ]
         },
         {
            "label":"DOH",
            "data":[
               {
                  "month":1,
                  "value":4
               },
               {
                  "month":2,
                  "value":21
               },
               {
                  "month":3,
                  "value":17
               }
            ]
         },
         {
            "label":"YoY gr",
            "data":[
               {
                  "month":1,
                  "value":22
               },
               {
                  "month":2,
                  "value":6
               },
               {
                  "month":3,
                  "value":93
               }
            ]
         }
      ]
   },
   "rcpa":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"RCPA Sales",
            "data":[
               {
                  "month":1,
                  "value":1381857
               },
               {
                  "month":2,
                  "value":9550141
               },
               {
                  "month":3,
                  "value":2214291
               }
            ]
         },
         {
            "label":"% of Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":2
               },
               {
                  "month":2,
                  "value":70
               },
               {
                  "month":3,
                  "value":59
               }
            ]
         },
         {
            "label":"% of Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":27
               },
               {
                  "month":2,
                  "value":66
               },
               {
                  "month":3,
                  "value":2
               }
            ]
         }
      ]
   },
   "sales_plan":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"Sales Planned",
            "data":[
               {
                  "month":1,
                  "value":9456788
               },
               {
                  "month":2,
                  "value":8278045
               },
               {
                  "month":3,
                  "value":3672516
               }
            ]
         },
         {
            "label":"Sales target",
            "data":[
               {
                  "month":1,
                  "value":8200028
               },
               {
                  "month":2,
                  "value":1835577
               },
               {
                  "month":3,
                  "value":2261354
               }
            ]
         },
         {
            "label":"% Planned",
            "data":[
               {
                  "month":1,
                  "value":8
               },
               {
                  "month":2,
                  "value":58
               },
               {
                  "month":3,
                  "value":20
               }
            ]
         }
      ]
   }
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="brandwiseprimarysales">Brand Wise Primary Sales</h3>

<p>This endpoint gives primary sales figures according to brands</p>

<h4 id="url-3">URL</h4>

<pre><code>/BO/PrimarySales/BrandWise
</code></pre>

<h4 id="method-3">Method</h4>

<p>POST</p>

<h4 id="dataparams-3">Data Params</h4>

<h5 id="required-3">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-3">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"B2373 - Waelchi and Sons",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":27825,
            "value_2":3692,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":66,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":51,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":75,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B3774 - Funk, Fahey and Monahan",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":28033,
            "value_2":36139,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":89,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":90,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":96,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B6728 - Hyatt-Stamm",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":70827,
            "value_2":19087,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":83,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":17,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":14,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B5156 - Wunsch, Wolf and Kiehn",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":33492,
            "value_2":64517,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":46,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":21,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":20,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="skuwiseprimarysales">SKU Wise Primary Sales</h3>

<p>This endpoint gives primary sales figures according to SKUs</p>

<h4 id="url-4">URL</h4>

<pre><code>/BO/PrimarySales/SKUWise
</code></pre>

<h4 id="method-4">Method</h4>

<p>POST</p>

<h4 id="dataparams-4">Data Params</h4>

<h5 id="required-4">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>brand_id        = brand selected</li>
</ul>

<h6 id="optional-4">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"7065482 - Stokes-Leffler - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":40327,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":6,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":19,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":67,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"7046606 - Stokes-Leffler - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":97437,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":50,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":85,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":25,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"4001766 - Stokes-Leffler - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":10334,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":65,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":92,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":60,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"7355387 - Stokes-Leffler - SKU 4",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":72705,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":84,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":84,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="brandwisesecondarysales">Brand Wise Secondary Sales</h3>

<p>This endpoint gives secondary sales figures according to brands</p>

<h4 id="url-5">URL</h4>

<pre><code>/BO/SecondarySales/BrandWise
</code></pre>

<h4 id="method-5">Method</h4>

<p>POST</p>

<h4 id="dataparams-5">Data Params</h4>

<h5 id="required-5">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-5">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"B4894 - Bradtke-Mosciski",
         "secondary_title":"",
         "label_1":"Sec Sales ( Oct-74 )",
         "value_1":67408,
         "type_1":"curr",
         "section_1":{
            "label":"Sec Sales ( Oct-74 )",
            "value":63162,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":52,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Oct-74 )",
            "value":28900,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":23,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B5178 - Powlowski, Graham and Predovic",
         "secondary_title":"",
         "label_1":"Sec Sales ( Oct-74 )",
         "value_1":13173,
         "type_1":"curr",
         "section_1":{
            "label":"Sec Sales ( Oct-74 )",
            "value":57321,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":87,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Oct-74 )",
            "value":47515,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":34,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="skuwisesecondarysales">SKU Wise Secondary Sales</h3>

<p>This endpoint gives secondary sales figures according to SKUs</p>

<h4 id="url-6">URL</h4>

<pre><code>/BO/SecondarySales/SKUWise
</code></pre>

<h4 id="method-6">Method</h4>

<p>POST</p>

<h4 id="dataparams-6">Data Params</h4>

<h5 id="required-6">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>brand_id        = brand selected</li>
</ul>

<h6 id="optional-6">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"6984263 - Legros-Lesch - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Apr-73 ) ",
            "value":11800,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Apr-73 ) ",
            "value":8763,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":26,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"5685116 - Legros-Lesch - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Apr-73 ) ",
            "value":82558,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":18,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Apr-73 ) ",
            "value":2505,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":36,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"1363539 - Legros-Lesch - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Apr-73 ) ",
            "value":21899,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":5,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Apr-73 ) ",
            "value":5438,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":56,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="salesplan">Sales Plan</h3>

<p>This endpoint gives sales planned for last month and 2 perevious months </p>

<h4 id="url-7">URL</h4>

<pre><code>/BO/SalesPlan/Summary
</code></pre>

<h4 id="method-7">Method</h4>

<p>POST</p>

<h4 id="dataparams-7">Data Params</h4>

<h5 id="required-7">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>month_id        = month of which you want data returned</li>
</ul>

<h6 id="optional-7">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "hq_target":897729,
   "sales_target":884375,
   "sales_planned":359034,
   "sales_target_planned":69,
   "sc_doc_planned":88,
   "brands_planned":3,
   "sku_planned":44,
   "promoted_sku_planned":83,
   "new_prescribers_planned":5
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="salesplandoctorlist">Sales Plan Doctor List</h3>

<p>This endpoint gives doctors Sales Plan for the month </p>

<h4 id="url-8">URL</h4>

<pre><code>/BO/SalesPlan/DoctorsList
</code></pre>

<h4 id="method-8">Method</h4>

<p>POST</p>

<h4 id="dataparams-8">Data Params</h4>

<h5 id="required-8">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>month_id        = month of which you want data returned</li>

<li>query           = query term in the search box</li>
</ul>

<h6 id="optional-8">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "number":34,
         "doctor_id":659810,
         "doctor_name":"Deanna Satterfield",
         "doctor_speciality":"Travel Agent",
         "visit_category":"V2",
         "sales_planned":2985
      },
      {
         "number":90,
         "doctor_id":627589,
         "doctor_name":"Dameon Pouros",
         "doctor_speciality":"Data Processing Equipment Repairer",
         "visit_category":"V2",
         "sales_planned":9263
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="salesplanskulist">Sales Plan SKU List</h3>

<p>This endpoint gives doctors Sales Plan for the month </p>

<h4 id="url-9">URL</h4>

<pre><code>/BO/SalesPlan/SKUList
</code></pre>

<h4 id="method-9">Method</h4>

<p>POST</p>

<h4 id="dataparams-9">Data Params</h4>

<h5 id="required-9">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>month_id        = month of which you want data returned</li>

<li>query           = query term in the search box</li>
</ul>

<h6 id="optional-9">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "number":72,
         "sku_id":2855559,
         "sku_name":"SKU 1",
         "brand_name":"Veum, Heathcote and Kuvalis",
         "sales_planned":5594
      },
      {
         "number":71,
         "sku_id":906798,
         "sku_name":"SKU 2",
         "brand_name":"Champlin-Wisoky",
         "sales_planned":6394
      },
      {
         "number":59,
         "sku_id":8303625,
         "sku_name":"SKU 3",
         "brand_name":"Kerluke, Marks and Wunsch",
         "sales_planned":389
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="rcpadoctorstatus">RCPA Doctor Status</h3>

<p>This endpoint gives the RCPA doctor statuses</p>

<h4 id="url-10">URL</h4>

<pre><code>/BO/RCPA/DoctorStatus
</code></pre>

<h4 id="method-10">Method</h4>

<p>POST</p>

<h4 id="dataparams-10">Data Params</h4>

<h5 id="required-10">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>

<li>query_term      = search term typed ( Null by default )</li>
</ul>

<h6 id="optional-10">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "doctor_id":7190,
         "doctor_name":"Derek Gislason",
         "doctor_speciality":"Tailor",
         "visit_category":"V3",
         "sales_planned":6366,
         "status":2
      },
      {
         "id":2,
         "doctor_id":6826,
         "doctor_name":"Ms. Charlotte Smith PhD",
         "doctor_speciality":"Radiation Therapist",
         "visit_category":"V1",
         "sales_planned":1757,
         "status":1
      },
      {
         "id":3,
         "doctor_id":8986,
         "doctor_name":"Freda Nikolaus",
         "doctor_speciality":"Bellhop",
         "visit_category":"V2",
         "sales_planned":2953,
         "status":1
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="rcpadoctorlist">RCPA Doctor List</h3>

<p>This endpoint gives the list of </p>

<ul>
<li>doctors who are RCPAed</li>

<li>doctors who are not RCPAed</li>
</ul>

<h4 id="url-11">URL</h4>

<pre><code>/BO/RCPA/DoctorList
</code></pre>

<h4 id="method-11">Method</h4>

<p>POST</p>

<h4 id="dataparams-11">Data Params</h4>

<h5 id="required-11">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>
</ul>

<h6 id="optional-11">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "doctors_not_rcpaed":[
      {
         "number":82,
         "doctor_id":352359,
         "doctor_name":"Mr. Tyson Glover V",
         "doctor_speciality":"Corporate Trainer",
         "visit_category":"V1",
         "sales_planned":4960,
         "rcpa":null,
         "last_date_visited":2039,
         "no_of_visits":3
      },
      {
         "number":71,
         "doctor_id":787958,
         "doctor_name":"Prof. Dominic Ferry",
         "doctor_speciality":"Software Engineer",
         "visit_category":"V2",
         "sales_planned":2371,
         "rcpa":null,
         "last_date_visited":3392,
         "no_of_visits":2
      },
      {
         "number":46,
         "doctor_id":887060,
         "doctor_name":"Prof. Elouise Crooks",
         "doctor_speciality":"Home Appliance Repairer",
         "visit_category":"V1",
         "sales_planned":5563,
         "rcpa":null,
         "last_date_visited":2743,
         "no_of_visits":1
      }
   ],
   "doctors_rcpaed":[
      {
         "number":35,
         "doctor_id":458481,
         "doctor_name":"Laura Conroy",
         "doctor_speciality":"Transportation Equipment Painters",
         "visit_category":"V3",
         "sales_planned":9523,
         "rcpa":498,
         "last_date_visited":2986,
         "no_of_visits":2
      },
      {
         "number":41,
         "doctor_id":717840,
         "doctor_name":"Dr. Josefa Macejkovic I",
         "doctor_speciality":"Safety Engineer",
         "visit_category":"V2",
         "sales_planned":9776,
         "rcpa":6732,
         "last_date_visited":3553,
         "no_of_visits":1
      },
      {
         "number":62,
         "doctor_id":972985,
         "doctor_name":"Leila Kilback",
         "doctor_speciality":"Pharmaceutical Sales Representative",
         "visit_category":"V3",
         "sales_planned":7472,
         "rcpa":1112,
         "last_date_visited":5179,
         "no_of_visits":2
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="bototalcoverage">BO Total Coverage</h3>

<p>This endpoint gives the list of </p>

<ul>
<li>doctors not met</li>

<li>doctors met</li>
</ul>

<h4 id="url-12">URL</h4>

<pre><code>/BO/Coverage/TotalCoverage
</code></pre>

<h4 id="method-12">Method</h4>

<p>POST</p>

<h4 id="dataparams-12">Data Params</h4>

<h5 id="required-12">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>
</ul>

<h6 id="optional-12">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "doctors_not_met":[
      {
         "number":38,
         "doctor_id":3961,
         "doctor_name":"Timmothy Miller",
         "doctor_speciality":"Order Clerk",
         "visit_category":"V1",
         "sales_planned":1113,
         "rcpa":4662
      },
      {
         "number":21,
         "doctor_id":424492,
         "doctor_name":"Mustafa McGlynn",
         "doctor_speciality":"Securities Sales Agent",
         "visit_category":"V3",
         "sales_planned":2430,
         "rcpa":4123
      }
   ],
   "doctors_met":[
      {
         "number":92,
         "doctor_id":922701,
         "doctor_name":"Verdie Kovacek",
         "doctor_speciality":"Library Assistant",
         "visit_category":"V2",
         "sales_planned":2855,
         "rcpa":3648
      },
      {
         "number":77,
         "doctor_id":307653,
         "doctor_name":"Julianne Kunze",
         "doctor_speciality":"Casting Machine Operator",
         "visit_category":"V3",
         "sales_planned":6559,
         "rcpa":6331
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="bomultivisitcompliance">BO Multivisit compliance</h3>

<p>This endpoint gives the list of </p>

<ul>
<li>doctors with less compliance</li>

<li>doctors with 100% complaince</li>
</ul>

<h4 id="url-13">URL</h4>

<pre><code>/BO/MultiVisitCompliance/TotalCompliance
</code></pre>

<h4 id="method-13">Method</h4>

<p>POST</p>

<h4 id="dataparams-13">Data Params</h4>

<h5 id="required-13">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>
</ul>

<h6 id="optional-13">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "doctors_with_less":[
      {
         "number":52,
         "doctor_id":390680,
         "doctor_name":"Paolo Romaguera",
         "doctor_speciality":"Communication Equipment Repairer",
         "visit_category":"V3",
         "sales_planned":9278,
         "rcpa":4017,
         "last_date_visited":73,
         "no_of_visits":2
      },
      {
         "number":35,
         "doctor_id":360063,
         "doctor_name":"Mrs. Leda Rosenbaum",
         "doctor_speciality":"Life Science Technician",
         "visit_category":"V2",
         "sales_planned":9234,
         "rcpa":7673,
         "last_date_visited":8481,
         "no_of_visits":1
      }
   ],
   "doctors_with_100":[
      {
         "number":61,
         "doctor_id":343112,
         "doctor_name":"Dr. Angie Hoeger",
         "doctor_speciality":"Spotters",
         "visit_category":"V2",
         "sales_planned":4213,
         "rcpa":1158,
         "last_date_visited":9897,
         "no_of_visits":2
      },
      {
         "number":5,
         "doctor_id":359817,
         "doctor_name":"Rahsaan Gleason",
         "doctor_speciality":"Merchandise Displayer OR Window Trimmer",
         "visit_category":"V2",
         "sales_planned":1664,
         "rcpa":2390,
         "last_date_visited":3571,
         "no_of_visits":2
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<hr />

<h2 id="abmapis">ABM APIs</h2>

<h3 id="salessummary-1">Sales Summary</h3>

<p>This endpoint will return all data related to the 4 tiles available to the ABM</p>

<ul>
<li>Primary Sales</li>

<li>Secondary Sales</li>

<li>RCPA sales</li>

<li>PCPM</li>
</ul>

<h4 id="url-14">URL</h4>

<pre><code>/ABM/Dashboard/SalesSummary
</code></pre>

<h4 id="method-14">Method</h4>

<p>POST</p>

<h4 id="dataparams-14">Data Params</h4>

<h5 id="required-14">Required:</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>

<li>period          = mtd,qtd,ytd</li>

<li>month           = month selected</li>

<li>year           = year selected</li>
</ul>

<h6 id="optional-14">Optional</h6>

<p>N.A</p>

<h4 id="successresponse-1">Success Response</h4>

<pre><code>{
   "primary_sales":{
      "total_sales":148472,
      "date_range":"Apr-19",
      "sales_target":342035,
      "sales_growth":6,
      "target_achieved":74
   },
   "secondary_sales":{
      "total_sales":579409,
      "date_range":"Mar-19",
      "target_achieved":32
   },
   "pcpm":{
      "total_sales":480740,
      "date_range":"Apr-19",
      "target_achieved":59
   },
   "rcpa_sales":{
      "total_sales":381244,
      "target_achieved":11,
      "date_range":"Mar-19"
   }
}
</code></pre>

<p>Code: 200</p>

<h4 id="errorresponse-1">Error Response</h4>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="myefforts">My Efforts</h3>

<p>This endpoint will return all data related to the 3 graphs below the tiles</p>

<ul>
<li>My Doc Coverage</li>

<li>Joint Call Compliance</li>

<li>No of JFW Days</li>
</ul>

<p>It will also provide call average figures</p>

<h4 id="url-15">URL</h4>

<pre><code>/ABM/Dashboard/Efforts
</code></pre>

<h4 id="method-15">Method</h4>

<p>POST</p>

<h4 id="dataparams-15">Data Params</h4>

<h5 id="required-15">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>

<li>period          = mtd,qtd,ytd</li>

<li>month           = month selected</li>

<li>year            = year selected</li>
</ul>

<h6 id="optional-15">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "last_reporting":"05-Apr-08",
   "my_doc_coverage":26,
   "joint_call_compliance":90,
   "jfw_days":17,
   "call_average":36.1
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="trends-1">Trends</h3>

<p>This endpoint will return all trend related data. Trends by default shows 3 month of data. Can be configured</p>

<ul>
<li>Sales Summary</li>

<li>RCPA</li>

<li>Sales Plan</li>
</ul>

<h4 id="url-16">URL</h4>

<pre><code>/ABM/Dashboard/Trends
</code></pre>

<h4 id="method-16">Method</h4>

<p>POST</p>

<h4 id="dataparams-16">Data Params</h4>

<h5 id="required-16">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>
</ul>

<h6 id="optional-16">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "sales_summary":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":7976616
               },
               {
                  "month":2,
                  "value":6385182
               },
               {
                  "month":3,
                  "value":8411823
               }
            ]
         },
         {
            "label":"Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":8038960
               },
               {
                  "month":2,
                  "value":5569424
               },
               {
                  "month":3,
                  "value":4647792
               }
            ]
         },
         {
            "label":"Primary Target",
            "data":[
               {
                  "month":1,
                  "value":3811141
               },
               {
                  "month":2,
                  "value":7276831
               },
               {
                  "month":3,
                  "value":9078237
               }
            ]
         },
         {
            "label":"Achievement",
            "data":[
               {
                  "month":1,
                  "value":81
               },
               {
                  "month":2,
                  "value":56
               },
               {
                  "month":3,
                  "value":88
               }
            ]
         },
         {
            "label":"DOH",
            "data":[
               {
                  "month":1,
                  "value":57
               },
               {
                  "month":2,
                  "value":11
               },
               {
                  "month":3,
                  "value":33
               }
            ]
         },
         {
            "label":"YoY gr",
            "data":[
               {
                  "month":1,
                  "value":76
               },
               {
                  "month":2,
                  "value":55
               },
               {
                  "month":3,
                  "value":58
               }
            ]
         }
      ]
   },
   "rcpa":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"RCPA Sales",
            "data":[
               {
                  "month":1,
                  "value":9463949
               },
               {
                  "month":2,
                  "value":5681284
               },
               {
                  "month":3,
                  "value":3387124
               }
            ]
         },
         {
            "label":"% of Primary Sales",
            "data":[
               {
                  "month":1,
                  "value":42
               },
               {
                  "month":2,
                  "value":31
               },
               {
                  "month":3,
                  "value":31
               }
            ]
         },
         {
            "label":"% of Secondary Sales",
            "data":[
               {
                  "month":1,
                  "value":55
               },
               {
                  "month":2,
                  "value":19
               },
               {
                  "month":3,
                  "value":14
               }
            ]
         }
      ]
   },
   "sales_plan":{
      "months":[
         1,
         2,
         3
      ],
      "rows":[
         {
            "label":"GSP Planned",
            "data":[
               {
                  "month":1,
                  "value":6413875
               },
               {
                  "month":2,
                  "value":2820067
               },
               {
                  "month":3,
                  "value":8786613
               }
            ]
         },
         {
            "label":"GSP Target",
            "data":[
               {
                  "month":1,
                  "value":5895886
               },
               {
                  "month":2,
                  "value":7099418
               },
               {
                  "month":3,
                  "value":9911141
               }
            ]
         },
         {
            "label":"% Planned",
            "data":[
               {
                  "month":1,
                  "value":20
               },
               {
                  "month":2,
                  "value":88
               },
               {
                  "month":3,
                  "value":84
               }
            ]
         }
      ]
   }
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="hqwiseprimarysales">HQ Wise Primary Sales</h3>

<p>This endpoint gives primary sales figures according to HQs</p>

<h4 id="url-17">URL</h4>

<pre><code>/ABM/PrimarySales/HQWise
</code></pre>

<h4 id="method-17">Method</h4>

<p>POST</p>

<h4 id="dataparams-17">Data Params</h4>

<h5 id="required-17">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-17">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"Kozeyhaven",
         "secondary_title":"( # of BOs 7 )",
         "section_1":{
            "label":"Primary Sales",
            "value":6162,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":63,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":69,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":20,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"Lake Destanyport",
         "secondary_title":"( # of BOs 9 )",
         "section_1":{
            "label":"Primary Sales",
            "value":33084,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":61,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":37,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":57,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"South Trey",
         "secondary_title":"( # of BOs 5 )",
         "section_1":{
            "label":"Primary Sales",
            "value":13575,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":81,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":19,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":30,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"Ellisbury",
         "secondary_title":"( # of BOs 9 )",
         "section_1":{
            "label":"Primary Sales",
            "value":64396,
            "type":"curr"
         },
         "section_2":{
            "label":"Primary Ach",
            "value":10,
            "type":"perc"
         },
         "section_3":{
            "label":"Sec Sales ( Aug-02 )",
            "value":86,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":17,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="areabrandwiseprimarysales">Area Brand Wise Primary Sales</h3>

<p>This endpoint gives primary sales figures according to Brand Wise Area</p>

<h4 id="url-18">URL</h4>

<pre><code>/ABM/PrimarySales/BrandWiseArea
</code></pre>

<h4 id="method-18">Method</h4>

<p>POST</p>

<h4 id="dataparams-18">Data Params</h4>

<h5 id="required-18">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-18">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"B1899 - Okuneva-Powlowski",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":94108,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":69,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":11,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":86,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B8261 - Kovacek, Rice and Stark",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":47948,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":93,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":0,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":16,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B5666 - Jacobs PLC",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":23436,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":20,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":94,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":25,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B7790 - Klocko PLC",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":87687,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":25,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":79,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":53,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="brandwiseprimarysales-1">Brand Wise Primary Sales</h3>

<p>This endpoint gives primary sales figures according to brands</p>

<h4 id="url-19">URL</h4>

<pre><code>/ABM/PrimarySales/BrandWise
</code></pre>

<h4 id="method-19">Method</h4>

<p>POST</p>

<h4 id="dataparams-19">Data Params</h4>

<h5 id="required-19">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>hq_id           = selected hq</li>
</ul>

<h6 id="optional-19">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"B8061 - Watsica and Sons",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":59994,
            "value_2":27752,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":84,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":12,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":90,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B2264 - Ankunding, Gleason and Purdy",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":49391,
            "value_2":65477,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":79,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":34,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":92,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B3547 - Reilly, Streich and Hessel",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":93637,
            "value_2":61143,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":96,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":26,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":26,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B1051 - Hermann, Stehr and Witting",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales",
            "value":49156,
            "value_2":5751,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":99,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":81,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":99,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="skuwiseprimarysales-1">SKU Wise Primary Sales</h3>

<p>This endpoint gives primary sales figures according to SKUs</p>

<h4 id="url-20">URL</h4>

<pre><code>/ABM/PrimarySales/SKUWise
</code></pre>

<h4 id="method-20">Method</h4>

<p>POST</p>

<h4 id="dataparams-20">Data Params</h4>

<h5 id="required-20">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>brand_id        = brand selected</li>
</ul>

<h6 id="optional-20">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"5722051 - Rempel-Mann - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":50825,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":24,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":83,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":62,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"6169242 - Rempel-Mann - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":4057,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":59,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":48,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":32,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"5634614 - Rempel-Mann - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":59024,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":33,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":16,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":97,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"7524938 - Rempel-Mann - SKU 4",
         "secondary_title":"",
         "section_1":{
            "label":"Primary Sales (\u20b9)",
            "value":9147,
            "type":"curr"
         },
         "section_2":{
            "label":"Achievement",
            "value":90,
            "type":"perc"
         },
         "section_3":{
            "label":"% of total sales",
            "value":48,
            "type":"perc"
         },
         "section_4":{
            "label":"Primary YoY gr",
            "value":98,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="hqwisesecondarysales">HQ Wise Secondary Sales</h3>

<p>This endpoint gives secondary sales figures according to HQs</p>

<h4 id="url-21">URL</h4>

<pre><code>/ABM/SecondarySales/HQWise
</code></pre>

<h4 id="method-21">Method</h4>

<p>POST</p>

<h4 id="dataparams-21">Data Params</h4>

<h5 id="required-21">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-21">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"Brianaside",
         "secondary_title":"( # of BOs 5 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":29524,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":48,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":33036,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":34,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"Destinville",
         "secondary_title":"( # of BOs 4 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":63650,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":54,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":96851,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":74,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"Hestertown",
         "secondary_title":"( # of BOs 4 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":91487,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":14,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":20951,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":21,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"North Casperchester",
         "secondary_title":"( # of BOs 0 )",
         "section_1":{
            "label":"Sec Sales ( Nov-75 ) ",
            "value":30615,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Nov-75 ) ",
            "value":29126,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":42,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="areabrandwisesecondarysales">Area Brand Wise Secondary Sales</h3>

<p>This endpoint gives secondary sales figures according to Brand Wise Area</p>

<h4 id="url-22">URL</h4>

<pre><code>/ABM/SecondarySales/BrandWiseArea
</code></pre>

<h4 id="method-22">Method</h4>

<p>POST</p>

<h4 id="dataparams-22">Data Params</h4>

<h5 id="required-22">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-22">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"B9076 - Bahringer Ltd",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":63446,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":14,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":895,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":77,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B9644 - Dibbert-Rolfson",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":61091,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":87,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":50000,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":75,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B4385 - Yundt Inc",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":9562,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":34,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":80939,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":96,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B1511 - Kunde, Kohler and Marks",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Dec-96 ) ",
            "value":93729,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":70,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Dec-96 ) ",
            "value":21933,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":52,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="brandwisesecondarysales-1">Brand Wise Secondary Sales</h3>

<p>This endpoint gives secondary sales figures according to brands</p>

<h4 id="url-23">URL</h4>

<pre><code>/ABM/SecondarySales/BrandWise
</code></pre>

<h4 id="method-23">Method</h4>

<p>POST</p>

<h4 id="dataparams-23">Data Params</h4>

<h5 id="required-23">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>hq_id           = selected hq</li>
</ul>

<h6 id="optional-23">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"B9734 - Mueller LLC",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":86821,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":61,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":99848,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":31,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"B1730 - Wintheiser, Kiehn and Stehr",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":91673,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":11,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":91760,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":46,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"B7976 - Yost-Wintheiser",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":47516,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":71,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":22463,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":16,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"B1455 - Haley-Beatty",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jul-12 ) ",
            "value":71405,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":64,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jul-12 ) ",
            "value":69665,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":75,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="skuwisesecondarysales-1">SKU Wise Secondary Sales</h3>

<p>This endpoint gives secondary sales figures according to SKUs</p>

<h4 id="url-24">URL</h4>

<pre><code>/ABM/SecondarySales/SKUWise
</code></pre>

<h4 id="method-24">Method</h4>

<p>POST</p>

<h4 id="dataparams-24">Data Params</h4>

<h5 id="required-24">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>brand_id        = brand selected</li>
</ul>

<h6 id="optional-24">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"3737799 - Hickle and Sons - SKU 1",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":59156,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":3,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":34290,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":66,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"2011179 - Hickle and Sons - SKU 2",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":81703,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":62,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":62864,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":71,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"9204453 - Hickle and Sons - SKU 3",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":84266,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":76,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":46181,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":84,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"268868 - Hickle and Sons - SKU 4",
         "secondary_title":"",
         "section_1":{
            "label":"Sec Sales ( Jan-18 ) ",
            "value":89544,
            "type":"curr"
         },
         "section_2":{
            "label":"Sec YoY gr",
            "value":8,
            "type":"perc"
         },
         "section_3":{
            "label":"Primary Sales ( Jan-18 ) ",
            "value":32995,
            "type":"curr"
         },
         "section_4":{
            "label":"Sec as % of Primary",
            "value":96,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="bowisercpasales">BO Wise RCPA Sales</h3>

<p>This endpoint gives RCPA sales figures according to BOs</p>

<h4 id="url-25">URL</h4>

<pre><code>/ABM/RCPA/BOWise
</code></pre>

<h4 id="method-25">Method</h4>

<p>POST</p>

<h4 id="dataparams-25">Data Params</h4>

<h5 id="required-25">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-25">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "bo_id":2712,
         "bo_name":"Prof. Polly Fadel",
         "rcpa_sales":45666
      },
      {
         "id":2,
         "bo_id":3830,
         "bo_name":"Dr. Cortez Kuhlman MD",
         "rcpa_sales":69772
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<h3 id="hqwisepcpmsales">HQ Wise PCPM Sales</h3>

<p>This endpoint gives PCPM sales figures according to HQs</p>

<h4 id="url-26">URL</h4>

<pre><code>/ABM/PCPM/HQWise
</code></pre>

<h4 id="method-26">Method</h4>

<p>POST</p>

<h4 id="dataparams-26">Data Params</h4>

<h5 id="required-26">Required</h5>

<ul>
<li>user_id         = current logged in user</li>
</ul>

<h6 id="optional-26">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{
   "payload":[
      {
         "id":1,
         "title":"Port Terencemouth",
         "secondary_title":"( # of BOs 2 )",
         "section_1":{
            "label":"Primary Sales",
            "value":63689,
            "value_2":40597,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":619585,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":12065,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":81,
            "type":"perc"
         }
      },
      {
         "id":2,
         "title":"Maychester",
         "secondary_title":"( # of BOs 1 )",
         "section_1":{
            "label":"Primary Sales",
            "value":39276,
            "value_2":21108,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":446298,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":87392,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":97,
            "type":"perc"
         }
      },
      {
         "id":3,
         "title":"Jerrodmouth",
         "secondary_title":"( # of BOs 8 )",
         "section_1":{
            "label":"Primary Sales",
            "value":91682,
            "value_2":67973,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":327261,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":74034,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":46,
            "type":"perc"
         }
      },
      {
         "id":4,
         "title":"Jacobsonmouth",
         "secondary_title":"( # of BOs 4 )",
         "section_1":{
            "label":"Primary Sales",
            "value":32971,
            "value_2":40190,
            "type":"curr"
         },
         "section_2":{
            "label":"PCPM",
            "value":632739,
            "type":"curr"
         },
         "section_3":{
            "label":"Sec Sales ( Jul-84 ) ",
            "value":79290,
            "type":"curr"
         },
         "section_4":{
            "label":"PCPM YoY gr",
            "value":1,
            "type":"perc"
         }
      }
   ]
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />

<h3 id="aggregateefforts">Aggregate Efforts</h3>

<p>This endpoint will return all aggregated data related to the 3 graphs below the tiles</p>

<ul>
<li>Total Coverage</li>

<li>Multivisit Compliance</li>

<li>Docs RCPAed</li>
</ul>

<p>It will also provide aggregate call average figures</p>

<h4 id="url-27">URL</h4>

<pre><code>/ABM/Dashboard/AggregateEfforts
</code></pre>

<h4 id="method-27">Method</h4>

<p>POST</p>

<h4 id="dataparams-27">Data Params</h4>

<h5 id="required-27">Required</h5>

<ul>
<li>user_id         = current logged in user</li>

<li>display_user    = user whose data you want to see</li>

<li>period          = mtd,qtd,ytd</li>

<li>month           = month selected</li>

<li>year            = year selected</li>
</ul>

<h6 id="optional-27">Optional</h6>

<p>N.A</p>

<p>Success Response:</p>

<pre><code>{  
   "last_reporting":"25-May-75",
   "total_coverage":5,
   "multivisit_compliance":38,
   "docs_rcpa":63,
   "call_average":32.7
}
</code></pre>

<p>Code: 200 
Error Response:</p>

<pre><code>{}
</code></pre>

<p>Notes:</p>

<hr />

<hr />