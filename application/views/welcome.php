<!DOCTYPE html>
<html lang="en">
	<head>
		<!-- Required meta tags -->
		<meta charset="utf-8" />
		<meta
			name="viewport"
			content="width=device-width, initial-scale=1, shrink-to-fit=no"
		/>

		<!-- Bootstrap CSS -->
		<link
			rel="stylesheet"
			href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
			integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T"
			crossorigin="anonymous"
		/>

		<title>API SPECS</title>
		<style>
			pre {
				background-color: #d3d3d3;
			}
			h1,
			h2 {
				text-align: center;
			}
			hr {
				height: 3px;
				border: none;
				color: #7c7b7b;
				background-color: #7c7b7b;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
				
				<button
					class="navbar-toggler"
					type="button"
					data-toggle="collapse"
					data-target="#navbarSupportedContent"
					aria-controls="navbarSupportedContent"
					aria-expanded="false"
					aria-label="Toggle navigation"
				>
					<span class="navbar-toggler-icon"></span>
				</button>

				<div
					class="collapse navbar-collapse"
					id="navbarSupportedContent"
				>
					<ul class="navbar-nav mr-auto">
						
						
					</ul>
					<div class="nav-item dropdown" style="margin-right:100px">
						<a
							class="nav-link dropdown-toggle"
							href="#"
							id="navbarDropdown"
							role="button"
							data-toggle="dropdown"
							aria-haspopup="true"
							aria-expanded="false"
						>
							Quick Access
						</a>
						<div id="dropdown-content"
							class="dropdown-menu"
							aria-labelledby="navbarDropdown"
						>
							<a class="dropdown-item" href="#">Action</a>
							
						</div>
					</div>
				</div>
			</nav>
			<div class="row">
				<div class="container">
					<div class="wrapper">
						<?php
$this->load->view('specs'); ?>
					</div>
				</div>
			</div>
		</div>

		<!-- Optional JavaScript -->
		<!-- jQuery first, then Popper.js, then Bootstrap JS -->
		<script
			src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
			integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
			crossorigin="anonymous"
		></script>
		<script
			src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
			integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
			crossorigin="anonymous"
		></script>
		<script
			src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
			integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
			crossorigin="anonymous"
		></script>
		<script>
			var links = $('h3');
			var link_string = "";
			$.each(links,function(index,value){
				var text = $(value).text();
				var id = $(value).attr('id');
				link_string = link_string+'<a class="dropdown-item" href="#'+id+'">'+text+'</a>'
			});
			$('#dropdown-content').html(link_string);
			
		</script>
	</body>
</html>
